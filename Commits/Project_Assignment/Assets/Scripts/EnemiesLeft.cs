﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class EnemiesLeft : MonoBehaviour {

    public static int ELeft = 12;
    Text ERemaining;
    void Start () {
        ERemaining = GetComponent<Text>();
    }
	
	// Update is called once per frame
	void Update () {
        ERemaining.text = "" + ELeft;
    }
}
