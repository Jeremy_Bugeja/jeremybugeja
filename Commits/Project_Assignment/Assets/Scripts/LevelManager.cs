﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LevelManager : MonoBehaviour { 


    public static void LoadNextLevel()
    {
        Lives.livesCount = 3;
        CoinsLeft.CLeft = 7;
        EnemiesLeft.ELeft = 12;
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);

    }

    public void LoadLevel(string levelName)
    {
        print("Loading level " + levelName);
        SceneManager.LoadScene(levelName);
        Lives.livesCount = 3;
        CoinsLeft.CLeft = 7;
        EnemiesLeft.ELeft = 12;
    }

    public void QuitGame()
    {
        UnityEditor.EditorApplication.isPlaying = false;
    }

    public static void GameOver()
    {
        SceneManager.LoadScene(5);
    }

    public static void Level2()
    {
        SceneManager.LoadScene(2);
    }

    public static void Level3()
    {
        SceneManager.LoadScene(3);
    }

    void Update()
    {
        

    }
}