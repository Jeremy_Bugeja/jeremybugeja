﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class EndScreenResults3 : MonoBehaviour
{
    public Text TLVL1;

    
    // Use this for initialization
    void Start()
    {
        TLVL1 = GetComponent<Text>();

    }

    // Update is called once per frame
    void Update()
    {
        TLVL1.text = Timer.Level1Time;


    }
}
