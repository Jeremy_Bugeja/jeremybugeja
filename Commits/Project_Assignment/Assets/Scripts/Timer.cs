﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class Timer : MonoBehaviour {
    public Text Tpassed;
    public float seconds, minutes;
    public string LevelTime;
    public static string Level1Time;
    public static string Level2Time;
    public static string Level3Time;
    // Use this for initialization
    void Start () {
        Tpassed = GetComponent<Text>();
	}
	
	// Update is called once per frame
	void Update () {
        minutes = (int)(Time.timeSinceLevelLoad / 60f);
        seconds = (int)(Time.timeSinceLevelLoad % 60f);
        Tpassed.text = minutes.ToString("00") + ":" + seconds.ToString("00");
        LevelTime = minutes.ToString("00") + ":" + seconds.ToString("00");
        if (SceneManager.GetActiveScene().buildIndex == 1)
        {
            Level1Time = LevelTime;
        }
        if (SceneManager.GetActiveScene().buildIndex == 2)
        {
            Level2Time = LevelTime;
        }
        if (SceneManager.GetActiveScene().buildIndex == 3)
        {
            Level3Time = LevelTime;
        }
    }
}
