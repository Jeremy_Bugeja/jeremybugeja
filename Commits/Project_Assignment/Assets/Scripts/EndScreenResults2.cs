﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class EndScreenResults2 : MonoBehaviour
{

    public Text TLVL2;

    // Use this for initialization
    void Start()
    { 
        TLVL2 = GetComponent<Text>();

    }

    // Update is called once per frame
    void Update()
    {

        TLVL2.text = Timer.Level2Time;


    }
}
