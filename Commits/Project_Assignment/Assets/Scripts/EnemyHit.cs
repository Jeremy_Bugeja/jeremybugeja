﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyHit : MonoBehaviour {

    void OnCollisionEnter2D(Collision2D col)
    {
        if (col.gameObject.tag.Equals("Bullet"))
        {
            Destroy(gameObject);
            EnemiesLeft.ELeft -= 1;
            if (EnemiesLeft.ELeft < 1 && CoinsLeft.CLeft < 1)
            {
                LevelManager.Level2();
            }
        }
        if (col.gameObject.tag.Equals("Player"))
        {
            Lives.livesCount -= 1;
        }
    }
}
