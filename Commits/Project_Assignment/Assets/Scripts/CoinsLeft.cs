﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CoinsLeft : MonoBehaviour {
    public static int CLeft = 7;
    Text CRemaining;
	// Use this for initialization
	void Start () {
        CRemaining = GetComponent<Text>();
    }
	
	// Update is called once per frame
	void Update () {
        CRemaining.text ="" + CLeft;
	}
}
