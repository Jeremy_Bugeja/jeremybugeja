﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Coin : MonoBehaviour {

    void OnCollisionEnter2D(Collision2D coll)
    {
        if (coll.gameObject.tag.Equals("Player"))
        {
            CoinsLeft.CLeft -= 1;
            Destroy(gameObject);
            if (EnemiesLeft.ELeft < 1 && CoinsLeft.CLeft < 1)
            {
                LevelManager.LoadNextLevel();
            }
        }
    }
       
}
