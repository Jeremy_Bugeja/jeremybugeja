﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PowerTime : MonoBehaviour {

    public Text PowerN;
    public static float timeLeft = 10f;

    void Start () {
        PowerN = GetComponent<Text>();
    }

    // Update is called once per frame
    void Update() {
        while (timeLeft > 0)
        {
            if (PlayerMove.impact == 1)
            {
                PowerN.text = "Jump Boost Active";
            }
            if (PlayerMove.impact == 2)
            {
                PowerN.text = "Speed Boost Active";
            }
            if (PlayerMove.impact == 3)
            {
                PowerN.text = "Rate of Fire Boost Active";
            }
            timeLeft -= Time.deltaTime;
            if (timeLeft < 0)
            {
                timeLeft = 0;
                if (PlayerMove.impact == 1)
                {
                    PowerN.text = "--";
                }
                if (PlayerMove.impact == 2)
                {
                    PowerN.text = "--";
                }
                if (PlayerMove.impact == 3)
                {
                    PowerN.text = "--";
                }

            }
        }
       

    }
}
