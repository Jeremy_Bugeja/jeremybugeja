﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class EndScreenResults : MonoBehaviour {
    //public Text TLVL1;
   // public Text TLVL2;
    public Text TLVL3;
    // Use this for initialization
    void Start () {
        //TLVL1 = GetComponent<Text>();
        //TLVL2 = GetComponent<Text>();
        TLVL3 = GetComponent<Text>();
    }
	
	// Update is called once per frame
	void Update () {
        //TLVL1.text = Timer.Level1Time;
        //TLVL2.text = Timer.Level2Time;
       TLVL3.text = Timer.Level3Time;

    }
}
