﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraFollow : MonoBehaviour {

    public Transform PlayerChar;

	void FixedUpdate () {
        transform.position = new Vector3(PlayerChar.position.x, PlayerChar.position.y, transform.position.z);
	}
}
