﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Lives : MonoBehaviour {

    public static int livesCount = 3;
    Text LRemaining;
    // Use this for initialization
    void Start () {
        LRemaining = GetComponent<Text>();
	}
	
	// Update is called once per frame
	void Update () {
        LRemaining.text = "" + livesCount;
        if (livesCount < 1)
        {
            LevelManager.GameOver();
        }
    }
}
