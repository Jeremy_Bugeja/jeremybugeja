﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerMove : MonoBehaviour
{
    float playerPos;
    public string midjump = "n";
    public float speed = 10f;
    public float jump = 8f;
    char lastTurn;
    bool facingRight = true;
    public GameObject BulletRight, BulletLeft;
    Vector2 bulletPos;
    public GameObject PowerText;
    public static float PowerUpTime = 10f;
    public float fireRate = 0.5f;
    float nextBullet = 0.0f;
    public static int impact;
    

    void Start()
    {
    }

    void FixedUpdate()
    {
        if (Input.GetKeyDown(KeyCode.W) && (midjump == "n"))
        {
            GetComponent<Rigidbody2D>().velocity = new Vector3(0, jump, 0);
            midjump = "y";
        }

        if (Input.GetKey(KeyCode.D))
        {
            transform.Translate(-Vector3.left * speed * Time.deltaTime);
            lastTurn = 'd';
        }

        if (Input.GetKey(KeyCode.A))
        {
            transform.Translate(-Vector3.right * speed * Time.deltaTime);
            lastTurn = 'a';
        }

        if (Input.GetKey(KeyCode.Mouse0) && Time.time > nextBullet)
        {
            nextBullet = Time.time + fireRate;
            fire();
        }

    }

    void LateUpdate()
    {
        Vector3 localScale = transform.localScale;
        if (lastTurn == 'd')
        {
            facingRight = true;
        }
        else if (lastTurn == 'a')
        {
            facingRight = false;
        }
    }

    void OnCollisionEnter2D(Collision2D coll)
    {
        Vector3 normal = coll.contacts[0].normal;
        if (normal.y > 0)
        {
            midjump = "n";
        }
        if (coll.gameObject.tag.Equals("Insta"))
        {
            Lives.livesCount -= 3;
        }
        if (coll.gameObject.tag.Equals("JumpBoost"))
        {
            impact = 1;
            Destroy(coll.gameObject);
            StartCoroutine("JumpUpTimer");
        }
        if (coll.gameObject.tag.Equals("SpeedBoost"))
        {
            impact = 2;
            Destroy(coll.gameObject);
            StartCoroutine("SpeedUpTimer");
        }
        if (coll.gameObject.tag.Equals("FireRateBoost"))
        {
            impact = 3;
            Destroy(coll.gameObject);
            StartCoroutine("FireUpTimer");
        }
    }

    void fire()
    {
        bulletPos = transform.position;
        if (facingRight == true)
        {
            bulletPos += new Vector2(+1f, -0.35f);
            Instantiate(BulletRight, bulletPos, Quaternion.identity);
        }
        else
        {
            bulletPos += new Vector2(-1f, -0.35f);
            Instantiate(BulletLeft, bulletPos, Quaternion.identity);
        }
    }

    IEnumerator FireUpTimer()
    {
        PowerUpTime = 10f;
        while (PowerUpTime > 0f)
        {
            yield return new WaitForSeconds(1);
            PowerUpTime--;
            fireRate = 0.3f;
        }
        StopCoroutine("PowerUpTimer");
        fireRate = 0.5f;
    }

    IEnumerator JumpUpTimer()
    {
        PowerUpTime = 10f;
        while (PowerUpTime > 0f)
        {
            yield return new WaitForSeconds(1);
            PowerUpTime--;
            jump = 10f;
        }
        StopCoroutine("PowerUpTimer");
        jump = 8f;
    }

    IEnumerator SpeedUpTimer()
    {
        PowerUpTime = 10f;
        while (PowerUpTime > 0f)
        {
            yield return new WaitForSeconds(1);
            PowerUpTime--;
            speed = 12f;
        }
        StopCoroutine("PowerUpTimer");
        speed = 10f;
    }

}
